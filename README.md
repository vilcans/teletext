# Teletext utilities

`teletext` is a Rust crate for interpreting and rendering Teletext page content.

## Not yet implemented

- Features above presentation layer 1 as defined by the Teletext specification.
