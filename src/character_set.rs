//! Alphanumeric character sets.

/// A character set. In the case of Latin, a national subset should be selected.
#[derive(Debug, Clone, Copy)]
pub enum CharacterSet {
    Latin(NationalOption),
    CyrillicSerbianCroatian,  // Cyrillic - 1
    CyrillicRussianBulgarian, // Cyrillic - 2
    CyrillicUkranian,         // Cyrillic - 3
    Greek,
    Arabic,
    Hebrew,
}

/// A national option that defines some specific characters in the Latin character set.
#[derive(Debug, Clone, Copy)]
pub enum NationalOption {
    CzechSlovak,
    English,
    Estonian,
    French,
    German,
    Italian,
    LettishLithuanian,
    Polish,
    PortugueseSpanish,
    Romanian,
    SerbianCroatianSlovenian,
    SwedishFinnish,
    Turkish,
}

// Unicode characters are copied from the Wikipedia article:
// https://en.wikipedia.org/wiki/Teletext_character_set

const CZECH_SLOVAK_SUBSET: [char; 13] = [
    '#', 'ů', 'č', 'ť', 'ž', 'ý', 'í', 'ř', 'é', 'á', 'ě', 'ú', 'š',
];
const ENGLISH_SUBSET: [char; 13] = [
    '£', '$', '@', '←', '½', '→', '↑', '#', '─', '¼', '‖', '¾', '÷',
];
const ESTONIAN_SUBSET: [char; 13] = [
    '#', 'õ', 'Š', 'Ä', 'Ö', 'Ž', 'Ü', 'Õ', 'š', 'ä', 'ö', 'ž', 'ü',
];
const FRENCH_SUBSET: [char; 13] = [
    'é', 'ï', 'à', 'ë', 'ê', 'ù', 'î', '#', 'è', 'â', 'ô', 'û', 'ç',
];
const GERMAN_SUBSET: [char; 13] = [
    '#', '$', '§', 'Ä', 'Ö', 'Ü', '^', '_', '°', 'ä', 'ö', 'ü', 'ß',
];
const ITALIAN_SUBSET: [char; 13] = [
    '£', '$', 'é', '°', 'ç', '→', '↑', '#', 'ù', 'à', 'ò', 'è', 'ì',
];
const LETTISH_LITHUANIAN_SUBSET: [char; 13] = [
    '#', '$', 'Š', 'ė', 'ę', 'Ž', 'č', 'ū', 'š', 'ą', 'ų', 'ž', 'į',
];
const POLISH_SUBSET: [char; 13] = [
    '#', 'ń', 'ą', 'Ƶ', 'Ś', 'Ł', 'ć', 'ó', 'ę', 'ż', 'ś', 'ł', 'ź',
];
const PORTUGUESE_SPANISH_SUBSET: [char; 13] = [
    'ç', '$', '¡', 'á', 'é', 'í', 'ó', 'ú', '¿', 'ü', 'ñ', 'è', 'à',
];
const ROMANIAN_SUBSET: [char; 13] = [
    '#', '¤', 'Ţ', // Ţ or Ț
    'Â', 'Ş', // Ş or Ș
    'Ă', 'Î', 'ı', 'ţ', // ţ or ț
    'â', 'ş', // ş or ș
    'ă', 'î',
];
const SERBIAN_CROATIAN_SLOVENIAN_SUBSET: [char; 13] = [
    '#', 'Ë', 'Č', 'Ć', 'Ž', 'Đ', 'Š', 'ë', 'č', 'ć', 'ž', 'đ', 'š',
];
const SWEDISH_FINNISH_HUNGARIAN_SUBSET: [char; 13] = [
    '#', '¤', 'É', 'Ä', 'Ö', 'Å', 'Ü', '_', 'é', 'ä', 'ö', 'å', 'ü',
];
const TURKISH_SUBSET: [char; 13] = [
    '₺', 'ğ', 'İ', 'Ş', 'Ö', 'Ç', 'Ü', 'Ğ', 'ı', 'ş', 'ö', 'ç', 'ü',
];

/// Cyrillic - 1
const SERBIAN_CROATIAN_SET: [char; 0x60] = [
    ' ', '!', '"', '#', '$', '%', '&', '\'', '(', ')', '*', '+', ',', '-', '.', '/', '0', '1', '2',
    '3', '4', '5', '6', '7', '8', '9', ':', ';', '<', '=', '>', '?', 'Ч', 'А', 'Б', 'Ц', 'Д', 'Е',
    'Ф', 'Г', 'Х', 'И', 'Ј', 'К', 'Л', 'М', 'Н', 'О', 'П', 'Ќ', 'Р', 'С', 'Т', 'У', 'В', 'Ѓ', 'Љ',
    'Њ', 'З', 'Ћ', 'Ж', 'Ђ', 'Ш', 'Џ', 'ч', 'а', 'б', 'ц', 'д', 'е', 'ф', 'г', 'х', 'и', 'ј', 'к',
    'л', 'м', 'н', 'о', 'п', 'ќ', 'р', 'с', 'т', 'у', 'в', 'ѓ', 'љ', 'њ', 'з', 'ћ', 'ж', 'ђ', 'ш',
    '■',
];
/// Cyrillic - 2
const RUSSIAN_BULGARIAN_SET: [char; 0x60] = [
    ' ', '!', '"', '#', '$', '%', 'ы', '\'', '(', ')', '*', '+', ',', '-', '.', '/', '0', '1', '2',
    '3', '4', '5', '6', '7', '8', '9', ':', ';', '<', '=', '>', '?', 'Ю', 'А', 'Б', 'Ц', 'Д', 'Е',
    'Ф', 'Г', 'Х', 'И', 'Ѝ', 'К', 'Л', 'М', 'Н', 'О', 'П', 'Я', 'Р', 'С', 'Т', 'У', 'Ж', 'В', 'Ь',
    'Ъ', 'З', 'Ш', 'Э', 'Щ', 'Ч', 'Ы', 'ю', 'а', 'б', 'ц', 'д', 'е', 'ф', 'г', 'х', 'и', 'ѝ', 'к',
    'л', 'м', 'н', 'о', 'п', 'я', 'р', 'с', 'т', 'у', 'ж', 'в', 'ь', 'ъ', 'з', 'ш', 'э', 'щ', 'ч',
    '■',
];
/// Cyrillic - 3
const UKRANIAN_SET: [char; 0x60] = [
    ' ', '!', '"', '#', '$', '%', 'ї', '\'', '(', ')', '*', '+', ',', '-', '.', '/', '0', '1', '2',
    '3', '4', '5', '6', '7', '8', '9', ':', ';', '<', '=', '>', '?', 'Ю', 'А', 'Б', 'Ц', 'Д', 'Е',
    'Ф', 'Г', 'Х', 'И', 'Ѝ', 'К', 'Л', 'М', 'Н', 'О', 'П', 'Я', 'Р', 'С', 'Т', 'У', 'Ж', 'В', 'Ь',
    'І', 'З', 'Ш', 'Є', 'Щ', 'Ч', 'Ї', 'ю', 'а', 'б', 'ц', 'д', 'е', 'ф', 'г', 'х', 'и', 'ѝ', 'к',
    'л', 'м', 'н', 'о', 'п', 'я', 'р', 'с', 'т', 'у', 'ж', 'в', 'ь', 'і', 'з', 'ш', 'є', 'щ', 'ч',
    '■',
];

// The Arabic character at 0x26 is the replacement character (�) in the Wikipedia article, with the commend "unidentified"
const ARABIC_SET: [char; 0x60] = [
    ' ', '!', '"', '£', '$', '%', '�', 'ﻱ', ')', '(', '*', '+', ',', '-', '.', '/', '0', '1', '2',
    '3', '4', '5', '6', '7', '8', '9', ':', '؛', '>', '=', '<', '؟', 'ﺔ', 'ﺀ', 'ﺒ', 'ﺏ', 'ﺘ', 'ﺕ',
    'ﺎ', 'ﺍ', 'ﺑ', 'ﺓ', 'ﺗ', 'ﺛ', 'ﺟ', 'ﺣ', 'ﺧ', 'ﺩ', 'ﺫ', 'ﺭ', 'ﺯ', 'ﺳ', 'ﺷ', 'ﺻ', 'ﺿ', 'ﻃ', 'ﻇ',
    'ﻋ', 'ﻏ', 'ﺜ', 'ﺠ', 'ﺤ', 'ﺨ', '#', 'ـ', 'ﻓ', 'ﻗ', 'ﻛ', 'ﻟ', 'ﻣ', 'ﻧ', 'ﻫ', 'ﻭ', 'ﻰ', 'ﻳ', 'ﺙ',
    'ﺝ', 'ﺡ', 'ﺥ', 'ﻴ', 'ﻯ', 'ﻌ', 'ﻐ', 'ﻔ', 'ﻑ', 'ﻘ', 'ﻕ', 'ﻙ', 'ﻠ', 'ﻝ', 'ﻤ', 'ﻡ', 'ﻨ', 'ﻥ', 'ﻻ',
    '■',
];

const HEBREW_SET: [char; 0x60] = [
    ' ', '!', '"', '£', '$', '%', '&', '\'', '(', ')', '*', '+', ',', '-', '.', '/', '0', '1', '2',
    '3', '4', '5', '6', '7', '8', '9', ':', ';', '<', '=', '>', '?', '@', 'A', 'B', 'C', 'D', 'E',
    'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X',
    'Y', 'Z', '←', '½', '→', '↑', '#', 'א', 'ב', 'ג', 'ד', 'ה', 'ו', 'ז', 'ח', 'ט', 'י', 'ך', 'כ',
    'ל', 'ם', 'מ', 'ן', 'נ', 'ס', 'ע', 'ף', 'פ', 'ץ', 'צ', 'ק', 'ר', 'ש', 'ת', '₪', '‖', '¾', '÷',
    '■',
];

const GREEK_SET: [char; 0x60] = [
    ' ', '!', '"', '#', '$', '%', '&', '\'', '(', ')', '*', '+', ',', '-', '.', '/', '0', '1', '2',
    '3', '4', '5', '6', '7', '8', '9', ':', ';', '«', '=', '»', '?', 'ΐ', 'Α', 'Β', 'Γ', 'Δ', 'Ε',
    'Ζ', 'Η', 'Θ', 'Ι', 'Κ', 'Λ', 'Μ', 'Ν', 'Ξ', 'Ο', 'Π', 'Ρ', 'ʹ', 'Σ', 'Τ', 'Υ', 'Φ', 'Χ', 'Ψ',
    'Ω', 'Ϊ', 'Ϋ', 'ά', 'έ', 'ή', 'ί', 'ΰ', 'α', 'β', 'γ', 'δ', 'ε', 'ζ', 'η', 'θ', 'ι', 'κ', 'λ',
    'μ', 'ν', 'ξ', 'ο', 'π', 'ρ', 'ς', 'σ', 'τ', 'υ', 'φ', 'χ', 'ψ', 'ω', 'ϊ', 'ϋ', 'ό', 'ύ', 'ώ',
    '■',
];

impl CharacterSet {
    pub fn get_char(&self, character_code: u8) -> char {
        let c = character_code;
        match self {
            Self::Latin(subset) => subset.get_char(character_code),
            Self::CyrillicSerbianCroatian => char_from_set(&SERBIAN_CROATIAN_SET, c),
            Self::CyrillicRussianBulgarian => char_from_set(&RUSSIAN_BULGARIAN_SET, c),
            Self::CyrillicUkranian => char_from_set(&UKRANIAN_SET, c),
            Self::Greek => char_from_set(&GREEK_SET, c),
            Self::Arabic => char_from_set(&ARABIC_SET, c),
            Self::Hebrew => char_from_set(&HEBREW_SET, c),
        }
    }
}

fn char_from_set(set: &[char; 0x60], character_code: u8) -> char {
    match character_code {
        0x20..=0x7f => set[character_code as usize - 0x20],
        c => c as char,
    }
}

impl NationalOption {
    pub fn get_char(&self, character_code: u8) -> char {
        let subset = match self {
            Self::CzechSlovak => &CZECH_SLOVAK_SUBSET,
            Self::English => &ENGLISH_SUBSET,
            Self::Estonian => &ESTONIAN_SUBSET,
            Self::French => &FRENCH_SUBSET,
            Self::German => &GERMAN_SUBSET,
            Self::Italian => &ITALIAN_SUBSET,
            Self::LettishLithuanian => &LETTISH_LITHUANIAN_SUBSET,
            Self::Polish => &POLISH_SUBSET,
            Self::PortugueseSpanish => &PORTUGUESE_SPANISH_SUBSET,
            Self::Romanian => &ROMANIAN_SUBSET,
            Self::SerbianCroatianSlovenian => &SERBIAN_CROATIAN_SLOVENIAN_SUBSET,
            Self::SwedishFinnish => &SWEDISH_FINNISH_HUNGARIAN_SUBSET,
            Self::Turkish => &TURKISH_SUBSET,
        };
        match character_code {
            0x23 => subset[0],
            0x24 => subset[1],
            0x40 => subset[2],
            0x5b => subset[3],
            0x5c => subset[4],
            0x5d => subset[5],
            0x5e => subset[6],
            0x5f => subset[7],
            0x60 => subset[8],
            0x7b => subset[9],
            0x7c => subset[10],
            0x7d => subset[11],
            0x7e => subset[12],
            0x7f => '■',
            c => c as char,
        }
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_common_latin() {
        assert_eq!(
            'A',
            CharacterSet::Latin(NationalOption::English).get_char(0x41)
        );
        assert_eq!(
            'A',
            CharacterSet::Latin(NationalOption::SwedishFinnish).get_char(0x41)
        );
    }

    #[test]
    fn test_latin_subsets() {
        assert_eq!(
            '£',
            CharacterSet::Latin(NationalOption::English).get_char(0x23)
        );
        assert_eq!(
            '#',
            CharacterSet::Latin(NationalOption::SwedishFinnish).get_char(0x23)
        );
        assert_eq!(
            'ç',
            CharacterSet::Latin(NationalOption::PortugueseSpanish).get_char(0x23)
        );
        assert_eq!(
            '÷',
            CharacterSet::Latin(NationalOption::English).get_char(0x7e)
        );
        assert_eq!(
            'ß',
            CharacterSet::Latin(NationalOption::German).get_char(0x7e)
        );
        assert_eq!(
            'ü',
            CharacterSet::Latin(NationalOption::Turkish).get_char(0x7e)
        );
    }

    #[test]
    fn test_nonlatin_sets() {
        assert_eq!('ί', CharacterSet::Greek.get_char(0x5f));
        assert_eq!('Ы', CharacterSet::CyrillicRussianBulgarian.get_char(0x5f));
        assert_eq!('Џ', CharacterSet::CyrillicSerbianCroatian.get_char(0x5f));
        assert_eq!('Ї', CharacterSet::CyrillicUkranian.get_char(0x5f));
    }
}
