//! Attributes that define the style and settings for the displayed characters.

/// An attribute that changes the display state, and occupies the space of one character.
#[derive(Debug, Copy, Clone, PartialEq, Eq)]
#[repr(u8)]
pub enum SpacingAttribute {
    Alpha(u8), // 0x00 to 0x07
    Flash = 0x08,
    Steady = 0x09,
    EndBox = 0x0a,
    StartBox = 0x0b,
    NormalSize = 0x0c,
    DoubleHeight = 0x0d,
    DoubleWidth = 0x0e,
    DoubleSize = 0x0f,
    Mosaic(u8), // 0x10 to 0x17
    Conceal = 0x18,
    ContiguousMosaic = 0x19,
    SeparatedMosaic = 0x1a,
    Esc = 0x1b,
    BlackBackground = 0x1c,
    NewBackground = 0x1d,
    HoldMosaics = 0x1e,
    ReleaseMosaics = 0x1f,
}

impl From<SpacingAttribute> for u8 {
    fn from(value: SpacingAttribute) -> Self {
        match value {
            SpacingAttribute::Alpha(color) => color,
            SpacingAttribute::Flash => 0x08,
            SpacingAttribute::Steady => 0x09,
            SpacingAttribute::EndBox => 0x0a,
            SpacingAttribute::StartBox => 0x0b,
            SpacingAttribute::NormalSize => 0x0c,
            SpacingAttribute::DoubleHeight => 0x0d,
            SpacingAttribute::DoubleWidth => 0x0e,
            SpacingAttribute::DoubleSize => 0x0f,
            SpacingAttribute::Mosaic(color) => 0x10 + color,
            SpacingAttribute::Conceal => 0x18,
            SpacingAttribute::ContiguousMosaic => 0x19,
            SpacingAttribute::SeparatedMosaic => 0x1a,
            SpacingAttribute::Esc => 0x1b,
            SpacingAttribute::BlackBackground => 0x1c,
            SpacingAttribute::NewBackground => 0x1d,
            SpacingAttribute::HoldMosaics => 0x1e,
            SpacingAttribute::ReleaseMosaics => 0x1f,
        }
    }
}

impl SpacingAttribute {
    /// Create from a byte. Returns None if the given byte is not a spacing attribute.
    ///
    /// The highest bit in the byte will be ignored, as some formats has this bit set on spacing attributes,
    /// while it is not required.
    pub fn from_byte(byte: u8) -> Option<Self> {
        let byte = byte & 0x7f;
        match byte {
            0x00..=0x07 => Some(SpacingAttribute::Alpha(byte)),
            0x08 => Some(SpacingAttribute::Flash),
            0x09 => Some(SpacingAttribute::Steady),
            0x0a => Some(SpacingAttribute::EndBox),
            0x0b => Some(SpacingAttribute::StartBox),
            0x0c => Some(SpacingAttribute::NormalSize),
            0x0d => Some(SpacingAttribute::DoubleHeight),
            0x0e => Some(SpacingAttribute::DoubleWidth),
            0x0f => Some(SpacingAttribute::DoubleSize),
            0x10..=0x17 => Some(SpacingAttribute::Mosaic(byte & 7)),
            0x18 => Some(SpacingAttribute::Conceal),
            0x19 => Some(SpacingAttribute::ContiguousMosaic),
            0x1a => Some(SpacingAttribute::SeparatedMosaic),
            0x1b => Some(SpacingAttribute::Esc),
            0x1c => Some(SpacingAttribute::BlackBackground),
            0x1d => Some(SpacingAttribute::NewBackground),
            0x1e => Some(SpacingAttribute::HoldMosaics),
            0x1f => Some(SpacingAttribute::ReleaseMosaics),
            _ => None,
        }
    }
}

#[cfg(test)]
mod test {
    use super::SpacingAttribute;

    #[test]
    fn byte_to_attribute_with_tuple_variant() {
        assert_eq!(
            SpacingAttribute::Mosaic(2),
            SpacingAttribute::from_byte(0x12u8).unwrap()
        );
        assert_eq!(
            SpacingAttribute::Mosaic(2),
            SpacingAttribute::from_byte(0x12u8).unwrap()
        );
    }

    #[test]
    fn byte_to_attribute_with_unit_variant() {
        assert_eq!(
            SpacingAttribute::DoubleHeight,
            SpacingAttribute::from_byte(0x0du8).unwrap()
        );
    }

    #[test]
    fn byte_to_attribute_with_invalid_value() {
        assert!(SpacingAttribute::from_byte(0x40u8).is_none());
    }

    #[test]
    fn unit_variant_to_byte() {
        assert_eq!(0x08u8, SpacingAttribute::Flash.into());
    }

    #[test]
    fn tuple_variant_to_byte() {
        assert_eq!(0x02u8, SpacingAttribute::Alpha(2).into());
        assert_eq!(0x13u8, SpacingAttribute::Mosaic(3).into());
    }
}
