use std::ops::RangeInclusive;

use crate::attribute::SpacingAttribute;

const SPACE: u8 = 0x20;

/// The range of characters that are shown even if mosaic is enabled.
/// The graphics in the spec seems to show that this should be `0x41..=0x5a`,
/// but that does not make sense to me.
/// The MR9735 chip documentation instead shows non-mosaic characters in
/// the range `0x40..=0x5f` which seems more likely.
const ALWAYS_ALPHA_RANGE: RangeInclusive<u8> = 0x40..=0x5f;

#[derive(Default, Clone, Debug, PartialEq)]
pub enum Size {
    #[default]
    Normal,
    /// Tall characters
    DoubleHeight,
    /// Wide characters (versions 2.5 and 3.5)
    DoubleWidth,
    /// Tall and wide characters (versions 2.5 and 3.5)
    DoubleSize,
}

#[derive(Default, Clone, Debug, Copy, PartialEq)]
pub enum MosaicRendering {
    #[default]
    Contiguous,
    Separated,
}

#[derive(Clone, Debug, PartialEq)]
pub struct Style {
    /// Foreground color, 0 to 7
    pub foreground: u8,
    /// flash (true) or steady (false)
    pub flash: bool,
    /// Between a Start Box and End Box code
    pub boxed: bool,
    /// Character size
    pub size: Size,
    /// Conceal
    pub conceal: bool,
    /// Whether mosaic rendering is enabled (G1 character set)
    pub mosaic_enabled: bool,
    /// Mosaic rendering style
    pub mosaic_rendering: MosaicRendering,
    /// Background color, 0 to 7
    pub background: u8,
}

impl Default for Style {
    fn default() -> Self {
        Self {
            foreground: 7,
            flash: false,
            boxed: false,
            size: Size::Normal,
            conceal: false,
            mosaic_enabled: false,
            mosaic_rendering: MosaicRendering::Contiguous,
            background: 0,
        }
    }
}

#[derive(Clone)]
pub struct State {
    pub style: Style,
    /// Show last mosaic instead of space on a spacing attribute (0x00-0x1f)
    pub hold_mosaics: bool,
    pub held_mosaic: (u8, MosaicRendering),
}

impl Default for State {
    fn default() -> Self {
        Self {
            style: Default::default(),
            hold_mosaics: false,
            held_mosaic: (SPACE, MosaicRendering::Contiguous),
        }
    }
}

/// One single display character; alphanumeric or mosaic.
#[derive(Debug)]
pub struct DisplayElement {
    pub character: u8,
    pub style: Style,
}

/// Whether an attribute affects the character is on, or whether it starts after.
enum Set {
    /// "Set-At", having immediate effect
    At,
    /// "Set-After", effect starting at the next character
    After,
}

impl State {
    pub fn new() -> Self {
        Self::default()
    }

    pub fn next(self, byte: u8) -> (DisplayElement, State) {
        let byte = byte & 0x7f;
        match SpacingAttribute::from_byte(byte) {
            Some(attribute) => {
                let (set, next) = transition(&self, attribute);
                let state = match set {
                    Set::At => &next,
                    Set::After => &self,
                };
                if state.hold_mosaics {
                    (
                        DisplayElement {
                            character: state.held_mosaic.0,
                            style: Style {
                                mosaic_enabled: true,
                                mosaic_rendering: state.held_mosaic.1,
                                ..state.style.clone()
                            },
                        },
                        next,
                    )
                } else {
                    (
                        DisplayElement {
                            character: SPACE,
                            style: state.style.clone(),
                        },
                        next,
                    )
                }
            }
            None => {
                let mut next = self.clone();
                let mosaic = self.style.mosaic_enabled && !ALWAYS_ALPHA_RANGE.contains(&byte);
                if mosaic {
                    next.held_mosaic = (byte, self.style.mosaic_rendering);
                }
                (
                    DisplayElement {
                        character: byte,
                        style: Style {
                            mosaic_enabled: mosaic,
                            ..self.style.clone()
                        },
                    },
                    next,
                )
            }
        }
    }
}

fn transition(from_state: &State, attribute: SpacingAttribute) -> (Set, State) {
    let set: Set;
    let mut next = from_state.clone();
    match attribute {
        SpacingAttribute::Alpha(color) => {
            set = Set::After;
            // Black foreground is only supported in version >= 2.5,
            // although the spec notes that "Black should be used with caution
            // as it is interpreted by some existing Level 1 and Level 1.5 decoders"
            if color != 0 {
                next.style.mosaic_enabled = false;
                next.style.foreground = color;
                next.style.conceal = false;
            }
        }
        SpacingAttribute::Flash => {
            set = Set::After;
            next.style.flash = true;
        }
        SpacingAttribute::Steady => {
            set = Set::At;
            next.style.flash = false;
        }
        SpacingAttribute::EndBox => {
            set = Set::After;
            next.style.boxed = false;
        }
        SpacingAttribute::StartBox => {
            set = Set::After;
            next.style.boxed = true;
        }
        SpacingAttribute::NormalSize => {
            set = Set::At;
            next.style.size = Size::Normal;
        }
        SpacingAttribute::DoubleHeight => {
            set = Set::After;
            next.style.size = Size::DoubleHeight;
        }
        SpacingAttribute::DoubleWidth => {
            // Version 2.5 and 3.5 only.
            set = Set::After;
            // Should not change in 1.0 so held mosaic isn't cleared
            //next.style.size = Size::DoubleWidth;
        }
        SpacingAttribute::DoubleSize => {
            // Version 2.5 and 3.5 only.
            set = Set::After;
            // Should not change in 1.0 so held mosaic isn't cleared
            //next.style.size = Size::DoubleSize;
        }
        SpacingAttribute::Mosaic(color) => {
            set = Set::After;
            // Black foreground is only supported in version >= 2.5,
            // although the spec notes that "Black should be used with caution
            // as it is interpreted by some existing Level 1 and Level 1.5 decoders"
            if color != 0 {
                next.style.mosaic_enabled = true;
                next.style.foreground = color;
                next.style.conceal = false;
            }
        }
        SpacingAttribute::Conceal => {
            set = Set::At;
            next.style.conceal = true;
        }
        SpacingAttribute::ContiguousMosaic => {
            set = Set::At;
            next.style.mosaic_rendering = MosaicRendering::Contiguous;
        }
        SpacingAttribute::SeparatedMosaic => {
            set = Set::At;
            next.style.mosaic_rendering = MosaicRendering::Separated;
        }
        SpacingAttribute::Esc => {
            set = Set::After;
            // TODO: Toggle between the first and second G0 sets
        }
        SpacingAttribute::BlackBackground => {
            set = Set::At;
            // TODO: Changes background to black, or according to spec:
            // "or to the current Full Row colour if black background colour
            // substitution is in operation"
            next.style.background = 0;
        }
        SpacingAttribute::NewBackground => {
            set = Set::At;
            next.style.background = from_state.style.foreground;
        }
        SpacingAttribute::HoldMosaics => {
            set = Set::At;
            next.hold_mosaics = true;
        }
        SpacingAttribute::ReleaseMosaics => {
            set = Set::After;
            next.hold_mosaics = false;
        }
    }
    // The "Held-Mosaic" character is reset to "SPACE" at the start of each
    // row, on a change of alphanumeric/mosaics mode or on a change of size.
    if next.style.size != from_state.style.size
        || next.style.mosaic_enabled != from_state.style.mosaic_enabled
    {
        next.held_mosaic = (SPACE, MosaicRendering::Contiguous);
    }
    (set, next)
}

/// Decode the bytes of a line into what should be displayed.
pub fn decode_line(bytes: &[u8]) -> Vec<DisplayElement> {
    let mut state = State::default();
    let mut result = Vec::with_capacity(bytes.len());
    for byte in bytes {
        let (c, next_state) = state.next(*byte);
        result.push(c);
        state = next_state;
    }
    result
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn alpha_characters() {
        let elements = decode_line(&[0x41, 0x42, 0x43]);
        assert_eq!(elements.len(), 3);
        assert_eq!(elements[0].character, b'A');
        assert_eq!(elements[1].character, b'B');
        assert_eq!(elements[2].character, b'C');
        assert_eq!(elements[0].style, Style::default());
        assert_eq!(elements[1].style, Style::default());
        assert_eq!(elements[2].style, Style::default());
    }

    // Example G.3.2 in ETS 300 706
    #[test]
    fn separated_mosaic_graphics() {
        let e = decode_line(&[0x17, 0x39, 0x1a, 0x39]);
        assert_eq!(e[0].character, SPACE);
        assert!(e[2].style.mosaic_enabled);

        assert_eq!(e[1].character, 0x39);
        assert!(e[1].style.mosaic_enabled);
        assert_eq!(e[1].style.mosaic_rendering, MosaicRendering::Contiguous);
        assert_eq!(e[1].style.foreground, 7);

        assert_eq!(e[2].character, SPACE);
        assert!(e[2].style.mosaic_enabled);

        assert_eq!(e[3].character, 0x39);
        assert!(e[3].style.mosaic_enabled);
        assert_eq!(e[3].style.mosaic_rendering, MosaicRendering::Separated);
        assert_eq!(e[3].style.foreground, 7);
    }
}
