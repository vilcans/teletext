use crate::{parse::DisplayElement, CharacterSet};

const MOSAIC_CHARS: [char; 0x40] = [
    ' ', '🬀', '🬁', '🬂', '🬃', '🬄', '🬅', '🬆', '🬇', '🬈', '🬉', '🬊', '🬋', '🬌', '🬍', '🬎', '🬏', '🬐', '🬑',
    '🬒', '🬓', '▌', '🬔', '🬕', '🬖', '🬗', '🬘', '🬙', '🬚', '🬛', '🬜', '🬝', '🬞', '🬟', '🬠', '🬡', '🬢', '🬣',
    '🬤', '🬥', '🬦', '🬧', '▐', '🬨', '🬩', '🬪', '🬫', '🬬', '🬭', '🬮', '🬯', '🬰', '🬱', '🬲', '🬳', '🬴', '🬵',
    '🬶', '🬷', '🬸', '🬹', '🬺', '🬻', '█',
];

/// Get the Unicode character corresponding to a Teletext character.
pub fn to_char(e: &DisplayElement, character_set: CharacterSet) -> char {
    if e.style.mosaic_enabled {
        mosaic_char(e.character).unwrap_or_else(|| character_set.get_char(e.character))
    } else {
        character_set.get_char(e.character)
    }
}

fn mosaic_char(c: u8) -> Option<char> {
    match c {
        0x20..=0x3f => Some(MOSAIC_CHARS[c as usize - 0x20]),
        0x60..=0x7f => Some(MOSAIC_CHARS[c as usize - 0x40]),
        _ => None,
    }
}
